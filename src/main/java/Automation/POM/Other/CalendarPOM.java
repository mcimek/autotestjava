package Automation.POM.Other;

import Automation.Other.CustomDate;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import java.util.*;

public class CalendarPOM {
    private WebDriver driver;

    @FindBy(how = How.CLASS_NAME, using = "ui-datepicker-month")
    private WebElement currentMonth;
    @FindBy(how = How.CLASS_NAME, using = "ui-datepicker-year")
    private WebElement currentYear;
    @FindBy(how = How.CSS, using = "a.ui-datepicker-prev")
    private WebElement previousMonth;
    @FindBy(how = How.CSS, using = "a.ui-datepicker-next")
    private WebElement nextMonth;

    @FindBy(how = How.CLASS_NAME, using = "ui-datepicker-calendar")
    private WebElement allTheDaysContainer;

    public CalendarPOM(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }


    public boolean isPageLoaded() {
        boolean check;

        check = currentMonth.isEnabled() &&
                currentYear.isEnabled() &&
                previousMonth.isEnabled() &&
                nextMonth.isEnabled() &&
                allTheDaysContainer.isEnabled();

        return check;
    }


    public void setDate(Calendar calendar) {
        setYear(calendar.get(Calendar.YEAR));
        setMonth(calendar.get(Calendar.MONTH) + 1);
        clickDay(calendar.get(Calendar.DAY_OF_MONTH));
    }

    public boolean setYear(int year) {
        int currentYearInt = Integer.parseInt(currentYear.getText());
        int numberOfIterations = Math.abs(currentYearInt - year);
        numberOfIterations = numberOfIterations * 12;

        for (int i = 0; i < numberOfIterations; i++) {
            if (currentYearInt == year) {
                break;
            } else if (currentYearInt > year) {
                previousMonth.click();
            } else {
                nextMonth.click();
            }

            currentYearInt = Integer.parseInt(currentYear.getText());
        }

        return Integer.parseInt(currentYear.getText()) == year;
    }

    public boolean setMonth(int month) {
        String expectedMonthName = CustomDate.getMonthName(month);

        String currentMonthName = currentMonth.getText();
        int currentMonthNumber = CustomDate.getMonthNumber(currentMonthName);

        int numberOfIterations = Math.abs(currentMonthNumber - month);

        for (int i = 0; i < numberOfIterations; i++) {
            if (currentMonthNumber > month) {
                previousMonth.click();
            } else {
                nextMonth.click();
            }

            currentMonthName = currentMonth.getText();
            currentMonthNumber = CustomDate.getMonthNumber(currentMonthName);
        }

        return currentMonth.getText().equals(expectedMonthName);
    }

    public boolean clickDay(int day) {
        List<WebElement> allDays = allTheDaysContainer.findElements(By.cssSelector("td[data-handler='selectDay']"));

        for (WebElement dayObject : allDays) {
            if (Integer.parseInt(dayObject.getText()) == day) {
                dayObject.click();
                break;
            }
        }

        return isPageLoaded();
    }
}
