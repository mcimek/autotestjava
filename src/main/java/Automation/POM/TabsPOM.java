package Automation.POM;

import Automation.POM.Abstract.AbstractPOM;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import java.util.ArrayList;
import java.util.List;

public class TabsPOM extends AbstractPOM {
    @FindBy(how = How.CSS, using = "ul[role='tablist']")
    private WebElement tabContainer;

    TabsPOM(WebDriver driver) {
        super(driver);
    }


    @Override
    public boolean isPageLoaded() {
        ArrayList<WebElement> listOfObjects = new ArrayList<>();
        listOfObjects.add(tabContainer);

        return allObjectsExist(listOfObjects) &&
                getDemoQAMenu().isPageLoaded();
    }


    public boolean selectTab(String tabName) {
        for (WebElement tab : getAllTabs()) {
            if (tab.getText().equals(tabName)) {
                tab.click();
            }
        }

        return getNameOfSelectedTab().equals(tabName);
    }

    private List<WebElement> getAllTabs() {
        return tabContainer.findElements(By.tagName("li"));
    }

    public String getNameOfSelectedTab() {
        String nameOfSelectedTab = null;

        for (WebElement tab : getAllTabs()) {
            if (tab.getAttribute("aria-selected").toLowerCase().equals("true")) {
                nameOfSelectedTab = tab.getText();
            }
        }

        return nameOfSelectedTab;
    }
}
