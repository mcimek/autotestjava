package Automation.POM;

import Automation.POM.Abstract.AbstractPOM;
import Automation.Other.CustomDate;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class DatepickerPOM extends AbstractPOM {
    @FindBy(how = How.ID, using = "datepicker1")
    private WebElement dateTextField;

    DatepickerPOM(WebDriver driver) {
        super(driver);
    }

    @Override
    public boolean isPageLoaded() {
        ArrayList<WebElement> listOfObjects = new ArrayList<>();
        listOfObjects.add(dateTextField);

        return allObjectsExist(listOfObjects) &&
                getDemoQAMenu().isPageLoaded();
    }


    public boolean setDate(Calendar calendar) {
        setDateUsingDatePicker(dateTextField, calendar);

        return dateTextField.getAttribute("value").equals(convertCalendarDate(calendar));
    }


    private String convertCalendarDate(Calendar calendar) {
        String monthName = CustomDate.getMonthName(calendar.get(Calendar.MONTH));

        return monthName + " " + calendar.get(Calendar.DAY_OF_MONTH) + ", " + calendar.get(Calendar.YEAR);
    }
}
