package Automation.POM;

import Automation.POM.Abstract.AbstractPOM;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import java.util.ArrayList;

public class DroppablePOM extends AbstractPOM {
    @FindBy(how = How.ID, using = "draggableview")
    private WebElement dragMeToMyTargetObject;
    @FindBy(how = How.ID, using = "droppableview")
    private WebElement dropHereObject;

    DroppablePOM(WebDriver driver) {
        super(driver);
    }


    @Override
    public boolean isPageLoaded() {
        ArrayList<WebElement> listOfObjects = new ArrayList<>();
        listOfObjects.add(dragMeToMyTargetObject);
        listOfObjects.add(dropHereObject);

        return allObjectsExist(listOfObjects) &&
                getDemoQAMenu().isPageLoaded();
    }


    public boolean dragAndDrop() {
        dragAndDrop(dragMeToMyTargetObject, dropHereObject);

        return IsWithin();
    }

    public boolean IsWithin() {
        int dragMeToMyTargetObjectX = dragMeToMyTargetObject.getLocation().x;
        int dragMeToMyTargetObjectY = dragMeToMyTargetObject.getLocation().y;
        int dragMeToMyTargetObjectWidth = dropHereObject.getSize().width;
        int dragMeToMyTargetObjectHeight = dropHereObject.getSize().height;
        int dropHereObjectX = dropHereObject.getLocation().x;
        int dropHereObjectY = dropHereObject.getLocation().y;
        int dropHereObjectWidth = dropHereObject.getSize().width;
        int dropHereObjectHeight = dropHereObject.getSize().height;

        return (dragMeToMyTargetObjectX + dragMeToMyTargetObjectWidth) > dropHereObjectX &&
                dragMeToMyTargetObjectX < (dropHereObjectX + dropHereObjectWidth) &&
                (dragMeToMyTargetObjectY + dragMeToMyTargetObjectHeight) > dropHereObjectY &&
                dragMeToMyTargetObjectY < (dropHereObjectY + dropHereObjectHeight);
    }

    public boolean isDropped() {
        return dropHereObject.getText().equals("Dropped!");
    }
}
