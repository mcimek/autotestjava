package Automation.POM;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.*;

import java.util.List;

public class DemoQAMenuPOM {
    private WebDriver driver;

    @FindBy(how = How.CLASS_NAME, using = "classname")
    private List<WebElement> singlecriterion;

    @FindBy(how = How.LINK_TEXT, using = "Registration")
    private WebElement registrationButton;
    @FindBy(how = How.LINK_TEXT, using = "Droppable")
    private WebElement droppableButton;
    @FindBy(how = How.LINK_TEXT, using = "Datepicker")
    private WebElement datepickerButton;
    @FindBy(how = How.LINK_TEXT, using = "Slider")
    private WebElement sliderButton;
    @FindBy(how = How.ID, using = "menu-item-98")
    private WebElement tabsButton;

    public DemoQAMenuPOM(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }


    boolean isPageLoaded() {
        boolean check;

        check = registrationButton.isEnabled() &&
                droppableButton.isEnabled() &&
                datepickerButton.isEnabled() &&
                sliderButton.isEnabled() &&
                tabsButton.isEnabled();

        return check;
    }


    public RegistrationPOM ClickRegistrationButton() {
        registrationButton.click();
        return new RegistrationPOM(driver);
    }

    public DroppablePOM ClickDroppableButton() {
        droppableButton.click();
        return new DroppablePOM(driver);
    }

    public DatepickerPOM ClickDatepickerButton() {
        datepickerButton.click();
        return new DatepickerPOM(driver);
    }

    public SliderPOM ClickSliderButton() {
        sliderButton.click();
        return new SliderPOM(driver);
    }

    public TabsPOM ClickTabsButton() {
        tabsButton.click();
        return new TabsPOM(driver);
    }
}
