package Automation.POM;

import Automation.POM.Abstract.AbstractPOM;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import java.util.ArrayList;

public class SliderPOM extends AbstractPOM {
    @FindBy(how = How.ID, using = "amount1")
    private WebElement minimumNumberOfBedrooms;

    @FindBy(how = How.ID, using = "slider-range-max")
    private WebElement sliderBar;

    private WebElement sliderSelector;

    SliderPOM(WebDriver driver) {
        super(driver);
        sliderSelector = sliderBar.findElement(By.tagName("span"));
    }


    @Override
    public boolean isPageLoaded() {
        ArrayList<WebElement> listOfObjects = new ArrayList<>();
        listOfObjects.add(minimumNumberOfBedrooms);
        listOfObjects.add(sliderBar);

        return allObjectsExist(listOfObjects) &&
                getDemoQAMenu().isPageLoaded();
    }


    public boolean setSlider(int valueToSet) {
        int valueToAdd = valueToSet - Integer.parseInt(minimumNumberOfBedrooms.getAttribute("value"));
        moveSlider(sliderSelector, valueToAdd);

        return Integer.parseInt(minimumNumberOfBedrooms.getAttribute("value")) == valueToSet;
    }
}
