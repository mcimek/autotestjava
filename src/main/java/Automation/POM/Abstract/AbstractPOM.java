package Automation.POM.Abstract;

import Automation.POM.DemoQAMenuPOM;
import Automation.POM.Other.CalendarPOM;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import java.nio.file.Paths;
import java.util.Calendar;
import java.util.List;

public abstract class AbstractPOM {
    protected WebDriver driver;
    private static DemoQAMenuPOM demoQAMenu;

    public static DemoQAMenuPOM getDemoQAMenu() {
        return demoQAMenu;
    }


    public AbstractPOM(WebDriver driver) {
        this.driver = driver;
        demoQAMenu = new DemoQAMenuPOM(driver);
        initObjects();
    }

    public abstract boolean isPageLoaded();


    protected boolean setTextField(WebElement textField, String value) {
        textField.sendKeys(value);

        return textField.getAttribute("value").equals(value);
    }

    protected boolean setTextField(WebElement textField, long value) {
        return setTextField(textField, Long.toString(value));
    }

    protected boolean setDropdownField(WebElement dropdownField, String value) {
        Select dropdown = new Select(dropdownField);
        dropdown.selectByVisibleText(value);

        return dropdownField.getAttribute("value").equals(value);
    }

    protected boolean setDropdownField(WebElement dropdownField, int value) {
        return setDropdownField(dropdownField, Long.toString(value));
    }

    protected boolean setRadioButton(List<WebElement> listOfRadioButtons, String value) {
        boolean check = false;

        for (WebElement radioButton : listOfRadioButtons) {
            if (radioButton.getAttribute("value").trim().toLowerCase().equals(value.trim().toLowerCase())) {
                radioButton.click();
                break;
            }
        }
        for (WebElement radioButton : listOfRadioButtons) {
            if (radioButton.getAttribute("value").trim().toLowerCase().equals(value.trim().toLowerCase())) {
                check = radioButton.isSelected();
            }
        }

        return check;
    }

    protected boolean setCheckBox(List<WebElement> listOfCheckBoxes, String value) {
        boolean check = false;

        for (WebElement checkBox : listOfCheckBoxes) {
            if (checkBox.getAttribute("value").trim().toLowerCase().equals(value.trim().toLowerCase())) {
                if (!checkBox.isSelected()) {
                    checkBox.click();
                    break;
                }
            }
        }

        for (WebElement checkBox : listOfCheckBoxes) {
            if (checkBox.getAttribute("value").trim().toLowerCase().equals(value.trim().toLowerCase())) {
                check = checkBox.isSelected();
            }
        }

        return check;
    }

    protected boolean setFile(WebElement chooseFileField, String filePath) {
        chooseFileField.sendKeys(filePath);

        String expectedFileName = Paths.get(filePath).getFileName().toString();
        String actualFileName = Paths.get(chooseFileField.getAttribute("value")).getFileName().toString();

        return actualFileName.equals(expectedFileName);
    }


    protected void dragAndDrop(WebElement objectToDrag, WebElement objectToDropOn) {
        Actions actionsBuilder = new Actions(driver);

        Action action = actionsBuilder.dragAndDrop(objectToDrag, objectToDropOn).build();
        action.perform();
    }

    protected void moveSlider(WebElement sliderObject, int valueToAdd) {
        Keys slidingDirection;
        if (valueToAdd < 0) {
            slidingDirection = Keys.ARROW_LEFT;
        } else {
            slidingDirection = Keys.ARROW_RIGHT;
        }

        Actions actionBuilder = new Actions(driver);
        Action action = actionBuilder.click(sliderObject).build();
        action.perform();

        actionBuilder = new Actions(driver);
        action = actionBuilder.sendKeys(slidingDirection).build();
        for (int i = 0; i < Math.abs(valueToAdd); i++) {
            action.perform();
        }
    }

    protected void setDateUsingDatePicker(WebElement objectWithDatePicker, Calendar calendar) {
        objectWithDatePicker.click();

        CalendarPOM calendarPOM = new CalendarPOM(driver);
        calendarPOM.setDate(calendar);
    }


    protected boolean objectExists(WebElement uiObject) {
        boolean check;

        check = uiObject.isEnabled();

        return check;
    }

    protected boolean allObjectsExist(List<WebElement> listOfObjects) {
        boolean check = false;

        for (WebElement uiObject : listOfObjects) {
            check = objectExists(uiObject);

            if (check) break;
        }

        return check;
    }


    protected void initObjects()
    {
        PageFactory.initElements(driver, this);
    }
}
