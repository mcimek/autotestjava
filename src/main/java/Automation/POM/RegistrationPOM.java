package Automation.POM;

import Automation.POM.Abstract.AbstractPOM;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import java.util.ArrayList;
import java.util.List;

public class RegistrationPOM extends AbstractPOM {
    @FindBy(how = How.ID, using = "name_3_firstname")
    private WebElement firstNameTextField;
    @FindBy(how = How.ID, using = "name_3_lastname")
    private WebElement lastNameTextField;
    @FindBy(how = How.NAME, using = "radio_4[]")
    private List<WebElement> maritalStatusRadioButtons;
    @FindBy(how = How.NAME, using = "checkbox_5[]")
    private List<WebElement> hobbyCheckBoxes;
    @FindBy(how = How.ID, using = "dropdown_7")
    private WebElement countryDropdown;
    @FindBy(how = How.ID, using = "mm_date_8")
    private WebElement monthOfBirthDropdown;
    @FindBy(how = How.ID, using = "dd_date_8")
    private WebElement dayOfBirthDropdown;
    @FindBy(how = How.ID, using = "yy_date_8")
    private WebElement yearOfBirthDropdown;
    @FindBy(how = How.ID, using = "phone_9")
    private WebElement phoneNumberTextField;
    @FindBy(how = How.ID, using = "username")
    private WebElement usernameTextField;
    @FindBy(how = How.ID, using = "email_1")
    private WebElement eMailTextField;
    @FindBy(how = How.ID, using = "profile_pic_10")
    private WebElement selectFileField;
    @FindBy(how = How.ID, using = "description")
    private WebElement aboutYourselfTextArea;
    @FindBy(how = How.ID, using = "password_2")
    private WebElement passwordTextField;
    @FindBy(how = How.ID, using = "confirm_password_password_2")
    private WebElement confirmPasswordTextField;
    @FindBy(how = How.NAME, using = "pie_submit")
    private WebElement submitButton;

    @FindBy(how = How.CLASS_NAME, using = "piereg_message")
    private WebElement positiveRegistrationConfirmation;
    @FindBy(how = How.CLASS_NAME, using = "piereg_login_error")
    private WebElement negativeRegistrationConfirmation;

    RegistrationPOM(WebDriver driver) {
        super(driver);
    }


    @Override
    public boolean isPageLoaded() {
        ArrayList<WebElement> listOfObjects = new ArrayList<>();
        listOfObjects.add(firstNameTextField);
        listOfObjects.add(lastNameTextField);
        listOfObjects.add(countryDropdown);
        listOfObjects.add(monthOfBirthDropdown);
        listOfObjects.add(dayOfBirthDropdown);
        listOfObjects.add(yearOfBirthDropdown);
        listOfObjects.add(phoneNumberTextField);
        listOfObjects.add(usernameTextField);
        listOfObjects.add(eMailTextField);
        listOfObjects.add(selectFileField);
        listOfObjects.add(aboutYourselfTextArea);
        listOfObjects.add(passwordTextField);
        listOfObjects.add(confirmPasswordTextField);
        listOfObjects.add(submitButton);

        return allObjectsExist(listOfObjects) &&
                getDemoQAMenu().isPageLoaded();
    }


    public boolean setFirstName(String firstName) {
        return setTextField(firstNameTextField, firstName);
    }

    public boolean setLastName(String lastName) {
        return setTextField(lastNameTextField, lastName);
    }

    public boolean setMaritialStatus(String maritialStatus) {
        return setRadioButton(maritalStatusRadioButtons, maritialStatus);
    }

    public boolean setHobby(String hobby) {
        return setCheckBox(hobbyCheckBoxes, hobby);
    }

    public boolean setCountry(String countryName) {
        return setDropdownField(countryDropdown, countryName);
    }

    public boolean setDateOfBirth(int day, int month, int year) {
        return setDayOfBirth(day) &&
                setMonthOfBirth(month) &&
                setYearOfBirth(year);
    }

    public boolean setDayOfBirth(int day) {
        return setDropdownField(dayOfBirthDropdown, day);
    }

    public boolean setMonthOfBirth(int month) {
        return setDropdownField(monthOfBirthDropdown, month);
    }

    public boolean setYearOfBirth(int year) {
        return setDropdownField(yearOfBirthDropdown, year);
    }

    public boolean setPhoneNumber(long phoneNumber) {
        return setTextField(phoneNumberTextField, phoneNumber);
    }

    public boolean setUsername(String username) {
        return setTextField(usernameTextField, username);
    }

    public boolean setEmail(String emailAdress) {
        return setTextField(eMailTextField, emailAdress);
    }

    public boolean setProfilePicture(String picturePath) {
        return setFile(selectFileField, picturePath);
    }

    public boolean setAboutYourself(String description) {
        return setTextField(aboutYourselfTextArea, description);
    }

    public boolean setPassword(String password) {
        return setTextField(passwordTextField, password);
    }

    public boolean setConfirmPassword(String password) {
        return setTextField(confirmPasswordTextField, password);
    }

    public boolean clickSubmitButton() {
        submitButton.click();
        return objectExists(positiveRegistrationConfirmation);
    }
}
