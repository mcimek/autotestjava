package Automation.POM;

import Automation.POM.Abstract.AbstractPOM;
import org.openqa.selenium.WebDriver;

public class HomePOM extends AbstractPOM {
    public HomePOM(WebDriver driver) {
        super(driver);
    }

    @Override
    public boolean isPageLoaded() {
        return getDemoQAMenu().isPageLoaded();
    }
}
