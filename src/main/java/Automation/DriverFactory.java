package Automation;

import Automation.Other.CustomDirectoryNavigation;
import com.sun.jna.Library;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.*;
import org.openqa.selenium.chrome.*;

import java.util.concurrent.TimeUnit;

public class DriverFactory {
    private static WebDriver driver;

    public static WebDriver createDriver(String browserName) {
        switch (browserName) {
            case "Firefox":
                System.setProperty("webdriver.gecko.driver", getFirefoxDriverPath());
                driver = new FirefoxDriver();
                break;
            case "Chrome":
                System.setProperty("webdriver.chrome.driver", getChromeDriverPath());

                ChromeOptions options = new ChromeOptions();
                options.addArguments("disable-infobars");
                driver = new ChromeDriver(options);
                break;
        }

        driver.manage().window().maximize();

        driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);

        return driver;
    }

    private static String getFirefoxDriverPath(){
        return CustomDirectoryNavigation.getResourcesDirectory() + "\\SeleniumDrivers\\geckodriver.exe";
    }
    private static String getChromeDriverPath(){
        return CustomDirectoryNavigation.getResourcesDirectory() + "\\SeleniumDrivers\\chromedriver.exe";
    }


    public static void navigateToUrl(String url) {
        driver.navigate().to(url);
    }

    public static void closeBrowser() {
        driver.close();
    }
}
