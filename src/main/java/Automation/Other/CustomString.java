package Automation.Other;

import java.util.Random;

public class CustomString {
    public static String getRandomString(int numberOfCharacters) {
        String characters = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
        StringBuilder result = new StringBuilder(numberOfCharacters);

        Random random = new Random();

        for (int i = 0; i < numberOfCharacters; i++) {
            result.append(characters.charAt(random.nextInt(characters.length())));
        }

        return result.toString();
    }
}
