package Automation.Other;

public class CustomDirectoryNavigation {
    public static String getProjectDirectory() {
        return System.getProperty("user.dir");
    }

    public static String getResourcesDirectory() {
        return CustomDirectoryNavigation.getProjectDirectory() + "\\src\\main\\resources\\";
    }
}
