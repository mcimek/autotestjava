package Automation.Other;

import java.util.Arrays;

public class CustomDate {
    public static String getMonthName(int monthNumber) {
        String[] monthNames = {"", "January", "February", "March", "April", "May", "June",
                "July", "August", "September", "October", "November", "December"};

        return monthNames[monthNumber];
    }

    public static int getMonthNumber(String monthName) {
        String[] monthNames = {"", "January", "February", "March", "April", "May", "June",
                "July", "August", "September", "October", "November", "December"};

        return Arrays.asList(monthNames).indexOf(monthName) + 1;
    }
}
