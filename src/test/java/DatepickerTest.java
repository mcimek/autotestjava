import Abstract.AbstractTest;
import Automation.POM.DatepickerPOM;
import Automation.POM.HomePOM;
import org.junit.Assert;
import org.junit.Test;

import java.util.Calendar;
import java.util.Date;

public class DatepickerTest extends AbstractTest {
    @Test
    public void Datepicker() {
        HomePOM homePageObject = new HomePOM(driver);
        Assert.assertTrue(homePageObject.isPageLoaded());

        DatepickerPOM datepickerPageObject = homePageObject.getDemoQAMenu().ClickDatepickerButton();
        Assert.assertTrue(datepickerPageObject.isPageLoaded());

        Calendar calendar = Calendar.getInstance();
        Assert.assertTrue(datepickerPageObject.setDate(calendar));

        calendar.add(Calendar.MONTH, 4);
        Assert.assertTrue(datepickerPageObject.setDate(calendar));

        calendar.add(Calendar.YEAR, 1);
        Assert.assertTrue(datepickerPageObject.setDate(calendar));

        Assert.assertTrue(datepickerPageObject.setDate(calendar));

        calendar.add(Calendar.MONTH, -2);
        Assert.assertTrue(datepickerPageObject.setDate(calendar));

        calendar.add(Calendar.YEAR, -1);
        Assert.assertTrue(datepickerPageObject.setDate(calendar));
    }
}
