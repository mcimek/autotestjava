import Abstract.AbstractTest;
import Automation.POM.HomePOM;
import Automation.POM.SliderPOM;
import org.junit.Assert;
import org.junit.Test;

public class SliderTest extends AbstractTest {
    @Test
    public void Slider() {
        HomePOM homePageObject = new HomePOM(driver);
        Assert.assertTrue(homePageObject.isPageLoaded());

        SliderPOM sliderPageObject = homePageObject.getDemoQAMenu().ClickSliderButton();
        Assert.assertTrue(sliderPageObject.isPageLoaded());

        Assert.assertTrue(sliderPageObject.setSlider(5));
        Assert.assertTrue(sliderPageObject.setSlider(3));
        Assert.assertTrue(sliderPageObject.setSlider(3));
    }
}
