import Abstract.AbstractTest;
import Automation.Other.CustomDirectoryNavigation;
import Automation.Other.CustomString;
import Automation.POM.HomePOM;
import Automation.POM.RegistrationPOM;
import org.junit.Assert;
import org.junit.Test;

public class RegistractionTest extends AbstractTest {
    @Test
    public void Registration() {
        HomePOM homePageObject = new HomePOM(driver);
        Assert.assertTrue(homePageObject.isPageLoaded());

        RegistrationPOM registrationPageObject = homePageObject.getDemoQAMenu().ClickRegistrationButton();
        Assert.assertTrue(registrationPageObject.isPageLoaded());

        String username = "zosia" + CustomString.getRandomString(5);
        String pictureProfile = CustomDirectoryNavigation.getResourcesDirectory() + "\\RegistrationProfilePicture\\profilepicture.bmp";

        Assert.assertTrue(registrationPageObject.setFirstName("Zosia"));
        Assert.assertTrue(registrationPageObject.setLastName("Samosia"));
        Assert.assertTrue(registrationPageObject.setMaritialStatus("Divorced"));
        Assert.assertTrue(registrationPageObject.setMaritialStatus("Divorced"));
        Assert.assertTrue(registrationPageObject.setMaritialStatus("Married"));
        Assert.assertTrue(registrationPageObject.setHobby("Reading"));
        Assert.assertTrue(registrationPageObject.setHobby("Cricket"));
        Assert.assertTrue(registrationPageObject.setHobby("Cricket"));
        Assert.assertTrue(registrationPageObject.setCountry("Italy"));
        Assert.assertTrue(registrationPageObject.setDateOfBirth(11, 3, 2001));
        Assert.assertTrue(registrationPageObject.setPhoneNumber(555666777888L));
        Assert.assertTrue(registrationPageObject.setUsername(username));
        Assert.assertTrue(registrationPageObject.setEmail(username + "@email.pl"));
        Assert.assertTrue(registrationPageObject.setProfilePicture(pictureProfile));
        Assert.assertTrue(registrationPageObject.setAboutYourself("bla bla bla"));
        Assert.assertTrue(registrationPageObject.setPassword("zlooo111222"));
        Assert.assertTrue(registrationPageObject.setConfirmPassword("zlooo111222"));

        Assert.assertTrue(registrationPageObject.clickSubmitButton());
    }
}
