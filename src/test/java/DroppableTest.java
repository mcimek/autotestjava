import Abstract.AbstractTest;
import Automation.POM.DroppablePOM;
import Automation.POM.HomePOM;
import org.junit.Assert;
import org.junit.Test;

public class DroppableTest extends AbstractTest {
    @Test
    public void Droppable() {
        HomePOM homePageObject = new HomePOM(driver);
        Assert.assertTrue(homePageObject.isPageLoaded());

        DroppablePOM droppablePageObject = homePageObject.getDemoQAMenu().ClickDroppableButton();
        Assert.assertTrue(droppablePageObject.isPageLoaded());

        Assert.assertFalse(droppablePageObject.isDropped());
        Assert.assertTrue(droppablePageObject.dragAndDrop());
        Assert.assertTrue(droppablePageObject.isDropped());
    }
}
