import Abstract.AbstractTest;
import Automation.POM.HomePOM;
import Automation.POM.TabsPOM;
import org.junit.Assert;
import org.junit.Test;

public class TabsTest extends AbstractTest {
    @Test
    public void Tabs() {
        HomePOM homePageObject = new HomePOM(driver);
        Assert.assertTrue(homePageObject.isPageLoaded());

        TabsPOM tabsPageObject = homePageObject.getDemoQAMenu().ClickTabsButton();
        Assert.assertTrue(tabsPageObject.isPageLoaded());

        Assert.assertTrue(tabsPageObject.selectTab("Tab 3"));
        Assert.assertTrue(tabsPageObject.selectTab("Tab 2"));
        Assert.assertTrue(tabsPageObject.selectTab("Tab 2"));
        Assert.assertEquals(tabsPageObject.getNameOfSelectedTab(), "Tab 2");
    }
}
