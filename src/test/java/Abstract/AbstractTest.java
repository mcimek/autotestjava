package Abstract;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.openqa.selenium.WebDriver;

import Automation.DriverFactory;

public abstract class AbstractTest {
    protected static WebDriver driver;

    @BeforeClass
    public static void openBrowser() {
        driver = DriverFactory.createDriver("Chrome");
        DriverFactory.navigateToUrl("http://www.demoqa.com");
    }

    @AfterClass
    public static void endTest() {
        DriverFactory.closeBrowser();
    }
}
